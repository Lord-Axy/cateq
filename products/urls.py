from django.conf.urls import url,include
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^home/',views.home,name='home'),
    url(r'^categories/',views.categories,name='categories'),
    url(r'^products/',views.products,name='products'),
    url(r'^product_detail/',views.product_detail,name='product_detail'),
    url(r'^faq/',views.faq,name='faq'),
    url(r'^gallery/',views.about,name='gallery'),
]
